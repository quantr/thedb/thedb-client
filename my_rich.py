#!/usr/bin/python3

from rich.console import Console
from rich.table import Table
from rich.text import Text

def showLogo():
    console = Console()
    console.print("@@@@@@@@@@@@  @@@@                          [red]############   ############[/red]")
    console.print("   @@@@@      @@@@  @@@@@        @@@@@@     [red] ###     ####   ###     ####[/red]")
    console.print("   @@@@@      @@@@@@@@@@@@@   @@@@@@@@@@@@  [red] ###      ###   ###     ####[/red]")
    console.print("   @@@@@      @@@@@    @@@@  @@@@     @@@@@ [red] ###      ###   ###########[/red]")
    console.print("   @@@@@      @@@@     @@@@  @@@@@@@@@@@@@@ [red] ###      ###   ###      ###[/red]")
    console.print("   @@@@@      @@@@     @@@@   @@@@@         [red] ###     ####   ###     ####[/red]")
    console.print("   @@@@@      @@@@     @@@@@    @@@@@@@@@@  [red]############    ###########[/red]")

def show_info(cpuName, cpuCore, cpuArch , cpuHz, totalMem, usedMem):
    console = Console()
    table = Table(show_header=False, header_style="green")
    table.add_column("xx")
    table.add_column("xx")
    table.add_row("CPU", cpuName)
    table.add_row("CPU Hz", cpuHz)
    table.add_row("CPU Cores", cpuCore)
    table.add_row("CPU Archeteture", cpuArch)
    mem = f"{usedMem!r} / {totalMem!r}"
    table.add_row("Memory", mem)
    console.print(table)