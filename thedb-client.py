#!/usr/bin/python3
import socket
import argparse
import json
import time
import sys
from my_rich import showLogo
from my_rich import show_info
from command import parse_thedb_query

#arg parser
parser = argparse.ArgumentParser(description='TheDB.')
parser.add_argument('-p',"--port",  default=1980 , help="port to theDB")
parser.add_argument('-n',"--hostname", default="localhost",  help="hostname of theDB")
parser.add_argument('-v','--version', action='version', version='version: 0.0.1')
args = parser.parse_args()

showLogo()

print("\nconnecting to "+args.hostname+":"+str(args.port))

#socket init / show table
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((args.hostname, int(args.port)))
    s.send("sysinfo".encode())
    res = s.recv(1024).decode()
    try:
        o = json.loads(res)
        show_info( o['name'], o['name'], o['name'] , o['name'], o['id'], o['id'])
    except json.JSONDecodeError as e:
        print("Invalid JSON syntax:", e)
except socket.error as e:
    print ("SYS INFO, Error connection")

#client cmd input
while True:
    #cmd = (sys.stdin.read().replace("\n", "\\n")+"\n")
    message = input(" -> ").strip()
    if(message.lower() != 'bye'):
        try:
            s.send(message.encode())
            data = s.recv(1024).decode()
            print("Server: "+data)
        except socket.error as e:
            print ("Error connection")
            break
    else:
        print("bye bye")
        break
    time.sleep(1)
s.close()

