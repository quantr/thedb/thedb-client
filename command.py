#!/usr/bin/python3

def parse_cmd(item):
    temp = []
    temp.append(f"S{len(item)}\n")
    temp.append(f"{item}\n")
    return temp

def parse_thedb_query(cmd):
    result = []
    arr = [x for x in cmd.split(' ') if x]
    result.append(f"+\n")
    result.append(f"A{len(arr)}\n")
    for idx, item in enumerate(arr, start=1):
        subArr = parse_cmd(item)
        result.append(subArr[0])
        result.append(subArr[1])

    result.append(f"-\n")
    return result

#print(parse_thedb_query("str_set name miles name peter name alex"))